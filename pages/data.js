export const dataSet ={
    "products": [
        {
            "id": 329,
            "n": "A zole Suspension",
            "v": "",
            "HSN": "",
            "ptype_id": 1,
            "category_id": "23,22",
            "sub_category_id": "0,0"
        },
        {
            "id": 288,
            "n": "Abelcet",
            "v": "",
            "HSN": "",
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 22,
            "n": "Ac Vet Fort D 2gm Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 18,
            "n": "Ac Vet Forte 3gm Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 19,
            "n": "Ac Vet Max 4.5gm Injection",
            "v": "",
            "HSN": "",
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 16,
            "n": "Ac-Vet Max 4.5gm Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1133,
            "n": "Aci Mix Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1134,
            "n": "Aci Mix Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 530,
            "n": "Aciloc 150mg Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 529,
            "n": "Aciloc 25 mg/ml Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 531,
            "n": "Aciloc 300mg Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 3,
            "n": "Acilox 1.5gm Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": 23,
            "sub_category_id": 0
        },
        {
            "id": 850,
            "n": "Acm 10mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 851,
            "n": "Acm 10mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 767,
            "n": "Acmegesic Forte Bolus",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 17,
            "n": "Acmelox 3gm Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 863,
            "n": "Add App 2mg/5ml Syrup",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 555,
            "n": "Ade Vit Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": 23,
            "sub_category_id": 0
        },
        {
            "id": 558,
            "n": "Advit 10ml Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1171,
            "n": "Advocate Spot-On Solution",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1287,
            "n": "Agrimin Forte Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1288,
            "n": "Agrimin Forte Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1289,
            "n": "Agrimin Forte Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1283,
            "n": "Agrimin I Bolus",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1284,
            "n": "Agrimin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1285,
            "n": "Agrimin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1286,
            "n": "Agrimin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 331,
            "n": "Albectin 3gm/100mg Bolus",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 321,
            "n": "Albomar 1.5gm Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 317,
            "n": "Albomar 25mg Suspension",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 318,
            "n": "Albomar 25mg Suspension",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 319,
            "n": "Albomar 25mg Suspension",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 320,
            "n": "Albomar 25mg Suspension",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 322,
            "n": "Albomar Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1010,
            "n": "Aldactone 100mg Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1008,
            "n": "Aldactone 25mg Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1009,
            "n": "Aldactone 50mg Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 861,
            "n": "Alert Vet Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 862,
            "n": "Alert Vet Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 802,
            "n": "Aletol 80mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 803,
            "n": "Aletol 80mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 305,
            "n": "Almizol Lotion",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 306,
            "n": "Almizol Lotion",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 711,
            "n": "Alphos 40 Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 712,
            "n": "Alphos 40 Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 275,
            "n": "Althrocin Fs 10 %",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 274,
            "n": "Althrocin Fs 20 %",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1282,
            "n": "Alvite-M",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1336,
            "n": "Ambiplex Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1337,
            "n": "Ambiplex Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1338,
            "n": "Ambiplex Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1339,
            "n": "Ambiplex Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1340,
            "n": "Ambiplex Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 289,
            "n": "Ambisome",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 267,
            "n": "Amidac 250mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 268,
            "n": "Amidac 250mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 269,
            "n": "Amidac 250mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 265,
            "n": "Amikin 250mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1231,
            "n": "Aminomin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1232,
            "n": "Aminomin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1233,
            "n": "Aminomin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1234,
            "n": "Aminomin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1235,
            "n": "Aminomin Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 40,
            "n": "Amoclox Forte Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1415,
            "n": "Amoxiclav 625 Tablet",
            "v": "",
            "HSN": 30049099,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1,
            "n": "Amoxirium 1.5gm Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 24,
            "n": "Amoxirium Forte 300mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 25,
            "n": "Amoxirum Forte 3gm Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 26,
            "n": "Amoxirum Forte 4.5gm Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 4,
            "n": "Amoxstron Powder 50%w/v",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 439,
            "n": "Amprolium",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 440,
            "n": "Amprolium",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1341,
            "n": "Anabolite-P Liquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1396,
            "n": "Analgon 1.5 gm Bolus",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1397,
            "n": "Analgon 3gm Bolus",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 943,
            "n": "Aneket 50mg/ml Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 944,
            "n": "Aneket 50mg/ml Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 945,
            "n": "Aneket 50mg/ml Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 843,
            "n": "Anistamin 10mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 844,
            "n": "Anistamin 10mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 845,
            "n": "Anistamin 10mg Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 355,
            "n": "Anti Worm",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 485,
            "n": "Anti-Diarrhea CompanyLiquid",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 451,
            "n": "Antrycide 2.5gm Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 502,
            "n": "Appetonic Forte Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1409,
            "n": "Appevet Bolus",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1240,
            "n": "Arbcrakkt",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 776,
            "n": "Artizone S 200mg/20mg/ml Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 777,
            "n": "Artizone S 200mg/20mg/ml Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1280,
            "n": "Ascal",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1277,
            "n": "Ascal Gold",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1278,
            "n": "Ascal Gold",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1279,
            "n": "Ascal Gold",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 1281,
            "n": "Ascal Pet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 414,
            "n": "Asuntol Soap",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 155,
            "n": "Ataxin 150mg Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 154,
            "n": "Ataxin 50mg Tablet",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 542,
            "n": "Atropine Sulphate Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 543,
            "n": "Atropine Sulphate Injection",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        },
        {
            "id": 441,
            "n": "Avatec Powder",
            "v": "",
            "HSN": null,
            "ptype_id": 1,
            "category_id": null,
            "sub_category_id": null
        }
    ],
    "discounts": [
        {
            "discount_id": 1,
            "product_id": 1419,
            "start_date": "2022-04-01",
            "end_date": "2039-03-01",
            "discount_type": "volume",
            "batch_number": null,
            "discount_value": 1,
            "qty": 2,
            "batch_type": "all batches",
            "deleted_date": null
        },
        {
            "discount_id": 2,
            "product_id": 1420,
            "start_date": "2022-04-19",
            "end_date": "2039-12-31",
            "discount_type": "volume",
            "batch_number": null,
            "discount_value": 1,
            "qty": 2,
            "batch_type": "all batches",
            "deleted_date": null
        }
    ],
    "category": [
        {
            "category_name": "Medicine",
            "category_id": 1,
            "parent_id": 0
        },
        {
            "category_name": "Feed",
            "category_id": 2,
            "parent_id": 2
        },
        {
            "category_name": "Accessories",
            "category_id": 3,
            "parent_id": 0
        },
        {
            "category_name": "Test Kits",
            "category_id": 4,
            "parent_id": 0
        },
        {
            "category_name": "Diagnostics",
            "category_id": 6,
            "parent_id": 4
        },
        {
            "category_name": "Feeds",
            "category_id": 10,
            "parent_id": 0
        },
        {
            "category_name": "Pet Food",
            "category_id": 11,
            "parent_id": 10
        },
        {
            "category_name": "Dog Food",
            "category_id": 12,
            "parent_id": 11
        },
        {
            "category_name": "Cat Food",
            "category_id": 13,
            "parent_id": 11
        },
        {
            "category_name": "Dog Treats",
            "category_id": 18,
            "parent_id": 11
        },
        {
            "category_name": "Cattle Feed",
            "category_id": 19,
            "parent_id": 10
        },
        {
            "category_name": "Restraints",
            "category_id": 20,
            "parent_id": 3
        },
        {
            "category_name": "Grooming",
            "category_id": 21,
            "parent_id": 3
        },
        {
            "category_name": "OTC",
            "category_id": 22,
            "parent_id": 1
        },
        {
            "category_name": "Prescription ",
            "category_id": 23,
            "parent_id": 1
        }
    ]
}